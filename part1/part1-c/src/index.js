import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const Hello = ({ name, age }) => {
  //const { name, age } = props
  const bornYear = () => new Date().getFullYear() - age

  return (
    <div>
      <p>
        Hello {name}, you are {age} years old
      </p>
      <p>So you were probably born in {bornYear()}.</p>
    </div>
  )
}

const Counter = (props) => {
  const [ counter, setCounter ] = useState(0)

  setTimeout(
    () => setCounter(counter + 1),
    1000
  )

  //const { counter } = props
  const handleClick = () => {
    console.log('clicked')
    setCounter(counter + 1)
  }
  console.log(counter)
  return (
    <div>
      <div>Time elapsed since render: {counter}</div>
      <button onClick={handleClick}>++</button>
      <button onClick={() => setCounter(0)}> 
        zero
      </button>
    </div>
  )
}

const App = () => {
  const name = 'Peter'
  const age = 10

  //console.log('rendering...', counter)

  return (
    <div>
      <h1>Greetings</h1>
      <Hello name="Maya" age={26 + 10} />
      <Hello name={name} age={age} />
      <Counter />
    </div>
  )
}


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
