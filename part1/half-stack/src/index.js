import React from 'react'
import ReactDOM from 'react-dom'

const Header = (props) => {
  return (
    <>
      <h1>{props.course}</h1>
    </>
  )
}

const Entry = (props) => {
  return (
    <>
      <p>
        {props.part}: {props.exerciseNum}
      </p>
    </>
  )
}

const Total = (props) => {
  let count = 0
  props.parts.forEach(value => { count += value.exercises })

  return (
    <><p>Total exercises: {count}</p></>
  )
}


const Contents = (props) => {
  /*   const items = []
    for (const [part, num] of props.parts) {
      items.push(<Content part={part} exerciseNum={num} />)
    } */

  /*  const items = []
     props.parts.forEach(part => {
      items.push(<Content part={part[0]} exerciseNum={part[1]} />)
    })
   */

  const items = props.parts.map(part => {
    return (<Entry key={part.name} part={part.name} exerciseNum={part.exercises} />)
  })

  return (
    <>{items}</>
  )
}


const App = () => {
  const course = {
    name: 'Half Stack Application Development',
    parts: [
      { name: 'Fundamentals', exercises: 10 },
      { name: 'Props', exercises: 7 },
      { name: 'State', exercises: 14 },
      { name: 'Javascript', exercises: 24 }
    ]
  }

  return (
    <div>
      <Header course={course['name']} />
      <Contents parts={course['parts']} />
      <Total parts={course['parts']} />
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))