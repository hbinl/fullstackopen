import React from 'react'
import ReactDOM from 'react-dom'


const Hello = (props) => {
  const now = new Date()
  const a = 10
  const b = 20
  const elem = (!props.age) ? "wow" : 
   <p>You are {props.age} years old.</p>
    
  return (
    <>
      <p>Hello {props.name}, it is {now.toString()}</p>
      {elem}
      <p>{a} plus {b} is <b>{a + b}</b></p>
    </>
  )
}


const App = () => {
  console.log("hello from component")
  const world = "world"

  return (
    <div>
      <h1>Greetings human.</h1>
      <Hello name="HB" age={26} />
      <Hello name={world} age="billions" />
      <Hello name="null" />
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))